from accounts.models import Profile
from django.contrib.auth.models import User
from django.utils import timezone
from rest_auth.serializers import LoginSerializer
from rest_framework import exceptions, serializers
from .models import *
from hatterene import constants
# from accounts.serializers import ProfileSerializer

import logging

MAX_MALE = 5
MAX_FEMALE = 5
MAX_NUM_SESSION = MAX_MALE + MAX_FEMALE


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class SessionSerializer(serializers.ModelSerializer):

    # Meta
    class Meta:
        model = Session
        fields = ('id', 'male_room_id', 'female_room_id', 'status', 'users', 'start_time', 'user_id', 'num_females', 'num_males')

    
    id = serializers.IntegerField(required=False)
    status = serializers.ChoiceField(choices=constants.SESSION_STATUS_CHOICE, required=False)
    male_room_id = serializers.IntegerField(required=False)
    female_room_id = serializers.IntegerField(required=False)
    start_time = serializers.DateTimeField(required=False)
    users = UserSerializer(read_only=True, many=True)
    user_id = serializers.IntegerField(required=False, write_only=True)
    num_females = serializers.IntegerField(required=False)
    num_males = serializers.IntegerField(required=False)


    def validate_male_room_id(self, male_room_id):
        room, created = Room.objects.get_or_create(
            room_id=male_room_id, is_available=True)
        if not created:
            if not room.is_available:
                return serializers.ValidationError(f"The room with id '{male_room_id}'' is not available")
        return male_room_id


    def validate_female_room_id(self, female_room_id):
        room, created = Room.objects.get_or_create(
            room_id=female_room_id, is_available=True)
        if not created:
            if not room.is_available:
                return serializers.ValidationError(f"The room with id '{female_room_id}'' is not available")
        return female_room_id


    def update_status(self):
        """
        Function to get status by number of user
        """
        num_male = self.instance.users.filter(profile__gender='female').count()
        num_female = self.instance.users.filter(profile__gender='male').count()

        # update number of males and females
        self.num_female = num_female
        self.num_male = num_male

        # update status
        if num_male + num_female >= MAX_NUM_SESSION:
            self.status = 4
        elif num_male >= MAX_MALE:
            self.status = 2
        elif num_female >= MAX_FEMALE:
            self.status = 3
        else: 
            self.status = 1
        return
    
    def validate_session_id(self, id):
        if id:
            self.instance = Session.objects.get(pk=id)

    def valilate_user_id(self, user_id):
        user = User.objects.get(pk=user_id)
        if not user:
            raise serializers.ValidationError("This user is not exist!")
        return user_id


    def validate(self, data):
        if 'id' not in data.keys() or 'user_id' not in data.keys():
            if 'male_room_id' not in data.keys() or 'female_room_id' not in data.keys() or 'start_time' not in data.keys():
                raise serializers.ValidationError("The session is not exits. room id's and start time are required !")
            return data
        return data


    # Create Session object
    def create(self, validated_data):
        session = Session(**validated_data)
        session.status = '1'
        session.save()
        return session
  

    # Update Session object
    def update(self, instance, validated_data):
        instance.start_time = validated_data.get(
            'start_time', instance.start_time)
        instance.male_room_id = validated_data.get(
            'male_room_id', instance.male_room_id)
        instance.female_room_id = validated_data.get(
            'female_room_id', instance.female_room_id)

        # get user
        user = User.objects.get(pk=validated_data.get("user_id"))

        # Full Session
        if instance.status == 4:
            raise serializers.ValidationError("Full Session") 

        # Check user_gender
        if user.profile.gender == "male":
            if instance.status == 2:
                raise serializers.ValidationError("Full Male!")
            else:
                instance.users.add(user)
        else:
            if instance.status == 3:
                raise serializers.ValidationError("Full Female!")
            else:
                instance.users.add(user)
                self.update_status()

        # update and save  
        self.update_status()
        instance.save()
        return instance


