# Generated by Django 3.0 on 2021-01-03 13:28

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('session_room', '0004_auto_20210103_0910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='session',
            name='female_room_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='male_room_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='start_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='status',
            field=models.CharField(blank=True, choices=[('1', 'available'), ('2', 'full_male'), ('3', 'full_female'), ('4', 'full')], max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='users',
            field=models.ManyToManyField(blank=True, null=True, related_name='sessions', to=settings.AUTH_USER_MODEL),
        ),
    ]
