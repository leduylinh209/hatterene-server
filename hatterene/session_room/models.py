from django.db import models
# from ..hatterene.models import (SignalSaveMixin, CreatedAbstractModel, ModifiedAbstractModel, TrackingAbstractModel)
from model_utils.models import TimeStampedModel
from hatterene import constants
from django.contrib.auth.models import User

# Create your models here.
class Session(TimeStampedModel):
    start_time = models.DateTimeField(null=True, blank=True)
    status = models.IntegerField(choices=constants.SESSION_STATUS_CHOICE,default=1,  null=False, blank=False)
    male_room_id = models.IntegerField(null=True, blank=True) 
    female_room_id = models.IntegerField(null=True, blank=True)
    users = models.ManyToManyField(User, related_name="sessions", null=True, blank=True)
    num_males = models.IntegerField(null=False, default=0)
    num_females = models.IntegerField(null=False, default=0)


class Call(TimeStampedModel):
    male = models.ForeignKey(User, on_delete=models.CASCADE, related_name="males")
    female = models.ForeignKey(User, on_delete=models.CASCADE, related_name="females")

class Room(TimeStampedModel):
    room_id = models.IntegerField(null=False)
    is_available = models.BooleanField(default=True, null=False)
