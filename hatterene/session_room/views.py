from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import serializers, status
# from django.core.serializers import serialize
from django.forms.models import model_to_dict
from .serializers import (
    SessionSerializer
)
from .models import Session

# Create your views here.


class SessionCreateView(ListCreateAPIView):
    serializer_class = SessionSerializer
    permission_classes = [AllowAny]
    queryset = Session.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        # filter sessions by user_id
        if 'user_id' in request.data.keys():
            user_id = request.data['user_id']
            self.queryset = queryset.filter(
                users__id=user_id
            )

        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        print("create session")
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        session = self.perform_create(serializer)
        res = model_to_dict(session)

        # convert user_list to JSON
        user_list = []
        for user in res['users']:
            user_list.append(model_to_dict(user))
        res['users'] = user_list
        
        # convert status to number
        res['status'] = int(res['status'])

        headers = self.get_success_headers(serializer.data)
        return Response(
            res,
            status=status.HTTP_201_CREATED,
            headers=headers,
        )

    def perform_create(self, serializer):
        return serializer.save()


create_session = SessionCreateView.as_view()
