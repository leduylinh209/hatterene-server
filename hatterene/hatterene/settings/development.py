from .base import *  # NOQA

ALLOWED_HOSTS = ["*"]

CELERY_BROKER_URL = "redis://redis"

#Sentry
RAVEN_CONFIG = {
    "dsn": "https://d63db2f5946e40469e1248a406f6526b@o484789.ingest.sentry.io/5538530",
    "release": "develop",
}