SESSION_STATUS_CHOICE = (
    (1, 'available'),
    (2, 'full_male'),
    (3, 'full_female'),
    (4, 'full')
)

SEX_CHOICES = (
    ('female', 'Female'),
    ('male', 'Male'),
    ('other', 'Other')
)

USER_STATUS = (
    ('processing', 'Processing'),
    ('assigned', 'Assigned')
)
