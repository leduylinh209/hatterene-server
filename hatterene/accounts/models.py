from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE
from model_utils.models import TimeStampedModel
# from ..hatterene.models import SignalSaveMixin
from hatterene import constants

# Create your models here.
class Profile(TimeStampedModel):
    """
    Profile extends Django User models
    """
    user = models.OneToOneField(User, on_delete=CASCADE)
    username = models.CharField(max_length=64, unique=True)
    gender = models.CharField(max_length=32, choices=constants.SEX_CHOICES)
    email = models.EmailField(unique=True)

    phone = models.CharField(max_length=32, unique=True, null=True)
    address = models.TextField(blank=True, null=True)
    personal_facebook = models.CharField(max_length=256, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    interest = models.TextField(null=True, blank=True, verbose_name="Interests")
    status = models.CharField(max_length=32, choices=constants.USER_STATUS, default='processing')

    @property
    def full_name(self):
        return f"{self.user.last_name} + ' ' + {self.user.first_name}"

    
    def __str__(self):
        return f"{self.username}"