from django.contrib.auth.models import User
from django.utils import timezone
from rest_auth.serializers import LoginSerializer
from rest_framework import exceptions, serializers
from hatterene.utils import standardize_phone_number
from hatterene import constants
from .models import Profile


class MyRegisterSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=64, min_length=6, required=True)
    email = serializers.CharField(max_length=32, required=True)
    name = serializers.CharField(max_length=60, required=True)
    gender = serializers.ChoiceField(choices=constants.SEX_CHOICES, required=True)
    date_of_birth = serializers.DateField(required=False)

    phone = serializers.CharField(max_length=16, min_length=10, required=False)
    address = serializers.CharField(max_length=512, required=False)
    personal_facebook = serializers.CharField(max_length=512, required=False)

    password1 = serializers.CharField(write_only=True, min_length=8)
    password2 = serializers.CharField(write_only=True, min_length=8)


    def validate_name(self, name):
        return name.strip().title()

    def validate_phone(self, phone):
        print("check phone")
        phone = standardize_phone_number(phone)
        if phone is None:
            raise serializers.ValidationError("This Phone Number is not valid")
        return phone

    def validate(self, data):
        print("validate")
        # Check password match
        if data["password1"] != data["password2"]:
            raise serializers.ValidationError("The two password fields didn't match.")


        # Check username
        if Profile.objects.filter(username=data["username"]).exists():
            raise serializers.ValidationError(
                "This User name is in use. Please Try Again!"
            )

        # Check email
        if Profile.objects.filter(email=data["email"]).exists():
            raise serializers.ValidationError(
                "This Email is in use. Please Try Again!"
            )

        return data

    def save(self, request):
        print("save")
        # If name has 1 word, it's first_name, else, first word is last_name
        name = self.validated_data["name"].split()
        last_name = ""
        first_name = name[0]
        if len(name) > 1:
            first_name = " ".join(name[1:])
            last_name = name[0]

        user, created = User.objects.get_or_create(
            email=self.validated_data["email"],
            defaults={"first_name": first_name, "last_name": last_name, "username": self.validated_data.get("username")},
        )



        # Reset password when user has no profile or newly created
        if created or not hasattr(user, "profile"):
            user.set_password(self.validated_data["password1"])
            user.last_login = timezone.now()
            user.save()


            Profile.objects.create(
                user=user,
                gender=self.validated_data.get("gender"),
                username=self.validated_data.get("username"),
                email=self.validated_data.get("email"),
                date_of_birth=self.validated_data.get("date_of_birth"),
                phone=self.validated_data.get("phone"),
                address=self.validated_data.get("address"),
                personal_facebook=self.validated_data.get("personal_facebook"),
            )

            return user

        raise serializers.ValidationError("This Phone Number is in use!")


class ProfileSerializer(serializers.ModelSerializer):

    date_of_birth = serializers.DateField(
        format="%Y-%m-%d",
        input_formats=["%d-%m-%Y", "%Y-%m-%d"],
        required=False,
        allow_null=True,
    )

class UpdateUserStatusSerializer(serializers.Serializer):

    def update(self, instance, validated_data):
        instance.profile.status = "assigned"
        instance.save()
        return instance

