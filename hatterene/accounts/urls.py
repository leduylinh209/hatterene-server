from django.urls import path
from . import views

urlpatterns = [
    path("register/", views.register_view),
    path("update/<int:pk>/", views.update_user_view),
]