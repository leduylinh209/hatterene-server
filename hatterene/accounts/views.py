from .serializers import (
    MyRegisterSerializer, UpdateUserStatusSerializer,
)

# from .tasks import slack_hook_new_signup
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters
from rest_auth.app_settings import TokenSerializer, create_token
from rest_auth.models import TokenModel
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import CreateAPIView, ListAPIView, ListCreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from django.forms.models import model_to_dict

# Create your views here.
class RegisterView(CreateAPIView):
    serializer_class = MyRegisterSerializer
    permission_classes = [AllowAny]
    token_model = TokenModel

    # @sensitive_post_parameters_m
    def dispatch(self, *args, **kwargs):
        return super(RegisterView, self).dispatch(*args, **kwargs)

    def get_response_data(self, user):
        return TokenSerializer(user.auth_token).data

    def create(self, request, *args, **kwargs):
        print("create user")
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            self.get_response_data(user),
            status=status.HTTP_201_CREATED,
            headers=headers,
        )

    def perform_create(self, serializer):
        user = serializer.save(self.request)
        create_token(self.token_model, user, serializer)
        # slack_hook_new_signup.delay(user.id)
        return user


class UpdateUserView(UpdateAPIView):
    serializer_class = UpdateUserStatusSerializer
    permission_classes = [AllowAny]
    queryset = User.objects.all()


    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        user = model_to_dict(self.perform_update(serializer).profile)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(user)

    def perform_update(self, serializer):
        return serializer.save()
        
update_user_view = UpdateUserView.as_view()
register_view = RegisterView.as_view()