FROM python:3.8-slim
ARG DJANGO_SETTINGS_MODULE
RUN echo ${DJANGO_SETTINGS_MODULE}

ENV PYTHONUNBUFFERED 1
MAINTAINER Linh Leduy <leduylinh209@gmail.com>

# Supervisor for Python & libaio1 for Oracle
RUN apt-get update && apt-get install -y supervisor && rm -rf /var/lib/apt/lists/*

# Install python lib dep
COPY requirements/base.txt ./
RUN pip3 install -r base.txt
RUN pip3 install gunicorn

# Add code folder
RUN mkdir /code
WORKDIR /code
ADD . /code/

# Supervisor configuration
COPY deploy/supervisor_docker.conf /etc/supervisor/conf.d/supervisor_docker.conf

# Set Djanbgo env
ENV DJANGO_SETTINGS_MODULE ${DJANGO_SETTINGS_MODULE}

# create unprivileged user
RUN adduser --disabled-password --gecos '' myuser

EXPOSE 8888

ENTRYPOINT ["/code/entrypoint.sh"]
