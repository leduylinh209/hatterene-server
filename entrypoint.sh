#!/bin/bash
cd hatterene/

python manage.py migrate
python manage.py collectstatic --noinput
python manage.py createcachetable

supervisord
